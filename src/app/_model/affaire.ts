export class Affaire {
    idAffaire: number;
    reference: string;
    titre: string;
    description: string;
    statut: number;
    documents:Document[] = new Array();
}
