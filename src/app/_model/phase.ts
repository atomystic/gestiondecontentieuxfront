import { Tache } from './tache';

export class Phase {
  
	 nom:string;
	idPhase:number;
	dateDebut:Date;
	dateFin:Date;
	tache:Tache = new Tache();
	
}
