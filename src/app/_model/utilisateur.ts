import { Role } from './role';
export class Utilisateur {
    idUtilisateur : number;
    email : string;
    nomUtilisateur :string;
    prenomUtilisateur:string ;
    motDePasse:string;
}
