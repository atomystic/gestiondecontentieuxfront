import { Component, OnInit } from '@angular/core';
import { Tribunal } from '../_model/tribunal';
import { TribunalService } from '../_service/tribunal.service';

@Component({
  selector: 'app-tribunal',
  templateUrl: './tribunal.component.html',
  styleUrls: ['./tribunal.component.css']
})
export class TribunalComponent implements OnInit {

  tribunals :Tribunal[];
  constructor(private tribunalService : TribunalService) { }

  ngOnInit(): void {

    this.tribunalService.findAllTribunal().subscribe(data=>{

this.tribunals = data;

    })
  }

}
