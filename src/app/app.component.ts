import { TokenStorageServiceService } from './_service/token-storage-service.service';
import { AfterViewChecked, Component, ElementRef, OnChanges, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import * as AOS from 'aos';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewChecked, OnInit, OnChanges {
  constructor(private rt: Router, private token: TokenStorageServiceService) { }
  @ViewChild("couleur1") couleur1: ElementRef;
  // @ViewChild("couleur2") couleur2: ElementRef;
  // @ViewChild("couleur3") couleur3: ElementRef;
  // @ViewChild("couleur4") couleur4: ElementRef;
  // @ViewChild("couleur5") couleur5: ElementRef;
  logged : Boolean;
  user :any;

  
  ngOnInit() {
    AOS.init();
    this.user = this.token.getUser()
    if(this.user){
      this.logged = true;

    }else{
      this.logged = false;
    }

  }

  ngOnChanges(){

    
  }
  ngAfterViewChecked() {

    if (this.rt.url.indexOf("/t%C3%A2ches") >= 0 || this.rt.url.indexOf("/tache") >= 0) {
      this.couleur1.nativeElement.style.backgroundColor = " rgb(246, 79, 93)";
    }
    else if (this.rt.url.indexOf("/tribunal") >= 0) {
      this.couleur1.nativeElement.style.backgroundColor = "rgb(180, 252, 4)";
    }

    else if (this.rt.url.indexOf("/utilisateur") >= 0) {
      this.couleur1.nativeElement.style.backgroundColor = "rgb(4, 4, 4)";
    }

    else if (this.rt.url.indexOf("/home") >= 0) {
      this.couleur1.nativeElement.style.backgroundColor = "rgb(61, 49, 147)";
    }


  }

  OnLogOut() {

    this.token.signOut();
    this.rt.navigate(['/home'])
    setTimeout(() => {
      location.reload();
    }, 0);
  }

}


