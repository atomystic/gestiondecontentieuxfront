import { Role } from './../_model/role';
import { Utilisateur } from './../_model/utilisateur';
import { TokenStorageServiceService } from './../_service/token-storage-service.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UtilisateurService } from '../_service/utilisateur.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  roles: Role[] = []
  currentUser: any;
  utilisateur: Utilisateur = new Utilisateur();
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  //  @ViewChild("modalBtn") modalBtn: ElementRef;
  @ViewChild("modalBg") modalBg: ElementRef;
  @ViewChild("modalClose") modalClose: ElementRef;
  constructor(private token: TokenStorageServiceService, private utilisateurService: UtilisateurService, private rt : Router) { }

  ngOnInit(): void {
  
    this.currentUser = this.token.getUser();
    console.log(this.currentUser.prenom)
    this.utilisateur  = {
      email: this.currentUser.email,
      idUtilisateur: this.currentUser.id,
      motDePasse:"",
      nomUtilisateur: this.currentUser.username,
      prenomUtilisateur: this.currentUser.prenom,
    }


  }
  hiddenForm() {
    this.modalBg.nativeElement.classList = "modal-bg";
  }
  displayForm() {
    this.modalBg.nativeElement.classList.add("bg-active");
  }

  updateUser() {

    this.utilisateurService.updateUtilisateur(this.utilisateur).subscribe((data) => {
   
      this.token.signOut();
      this.rt.navigate(["/home"])

    })

  }
}