import { TokenStorageServiceService } from './../_service/token-storage-service.service';
import { Utilisateur } from './../_model/utilisateur';
import { Component, OnInit } from '@angular/core';
import { UtilisateurService } from '../_service/utilisateur.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-utilisateur',
  templateUrl: './utilisateur.component.html',
  styleUrls: ['./utilisateur.component.css']
})
export class UtilisateurComponent implements OnInit {
  idUser: number;
  utilisateurs: Utilisateur[];
  mot;
  constructor(private utilisateurService: UtilisateurService, private rt: Router, private token: TokenStorageServiceService
  ) { }

  ngOnInit(): void {
    this.idUser = this.token.getUser().id
    this.utilisateurService.findAllUtilisateurs().subscribe(data => {

      this.utilisateurs = data
    })
  }
  onDelete(utilisateur: Utilisateur) {

    if (confirm("Etes-vous sûr de vouloir supprimer cette utilisateur?")) {
      this.utilisateurService.deleteUtilisateur(utilisateur.idUtilisateur).subscribe(data => {
        if (this.idUser === utilisateur.idUtilisateur) {
          this.token.signOut()
          this.rt.navigate(["/enregistrement"])
        }
      })
    }
  }


  chercherUtilisateur() {
    this.utilisateurService.findAllUtilisateurs().subscribe(data => {


      this.utilisateurs = data.filter(a => {
        let i = a.idUtilisateur.toString();
        return i.indexOf(this.mot) >= 0;
      })

    })
  }


}
