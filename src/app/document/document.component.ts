import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Affaire } from '../_model/affaire';
import { Document } from '../_model/document';
import { DocumentService } from '../_service/document.service';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {
  idAffaire: number;
  document: Document = new Document();
  constructor(private ar: ActivatedRoute, private documentService: DocumentService, private rt: Router) { }

  ngOnInit(): void {



    this.ar.queryParamMap.subscribe(param => {
      let idParam = parseInt(param.get("id"));
      if (idParam != undefined) {
        this.idAffaire = idParam;
      }

    })

  }

  addDocument() {


    this.document.dateCreation = new Date();
    // console.log(new Date())
    this.documentService.addDocument(this.idAffaire, this.document).subscribe(data => {
      this.rt.navigate(["/accueil"]);
    })
  }

}
