import { Component, OnInit } from '@angular/core';
import { Affaire } from 'src/app/_model/affaire';
import { AffaireService } from 'src/app/_service/affaire.service';

@Component({
  selector: 'app-affaire',
  templateUrl: './affaire.component.html',
  styleUrls: ['./affaire.component.css']
})
export class AffaireComponent implements OnInit {
  affaire: Affaire = new Affaire();
  mot: string;
  affaires: Affaire[] = [];
  nombreAffaireSelectionne: number = 0;
  showForm: Boolean = false;
  showInput: Boolean = false;
  statut: any[] = [
    { nombre: 1, stat: "En cours" },

    { nombre: 3, stat: "A suivre" }


  ]


  constructor(private affaireService: AffaireService) { }

  ngOnInit(): void {

    alert("Pour voir les détails d'une affaire, cliquer sur l'affaire en question")
    this.affaireService.findAllAffaire().subscribe(data => {
      // console.log(data.filter(x=>x.idAffaire==12));

      this.affaires = data.sort((e, f) => { return e.statut - f.statut; })
      this.nombreAffaireSelectionne = this.affaires.length;
    })
  }

  displayForm() {
    this.showForm = !this.showForm;
  }

  displayRecherche() {
    this.showInput = !this.showInput;
  }
  chercherAffaire() {

    this.affaireService.findAllAffaire().subscribe(data => {


      this.affaires = data.sort((e, f) => { return e.statut - f.statut; }).filter(a => {
        return a.reference.indexOf(this.mot) >= 0;
      })

      this.nombreAffaireSelectionne = this.affaires.length;
    })
  }

  addAffaire() {
    this.affaireService.addAffaire(this.affaire).subscribe(data => {
      console.log("affaire ajouté");
      location.reload();

    })
  }
  

}
