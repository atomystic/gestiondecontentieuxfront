import { Utilisateur } from './../../_model/utilisateur';
import { Route } from '@angular/compiler/src/core';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Affaire } from 'src/app/_model/affaire';
import { Document } from 'src/app/_model/document';
import { AffaireService } from 'src/app/_service/affaire.service';
import { DocumentService } from 'src/app/_service/document.service';
import { UtilisateurService } from 'src/app/_service/utilisateur.service';



@Component({
  selector: 'app-affaire-details',
  templateUrl: './affaire-details.component.html',
  styleUrls: ['./affaire-details.component.css']
})
export class AffaireDetailsComponent implements OnInit {


  @ViewChild("modalBtn") modalBtn: ElementRef;
  @ViewChild("modalBg") modalBg: ElementRef;
  @ViewChild("modalClose") modalClose: ElementRef;
  affaire: Affaire = new Affaire();
  utilisateurs : Utilisateur[];
  idAffaire: number;
  document: Document = new Document();
  constructor(private affaireService: AffaireService,
     private ar: ActivatedRoute, private rt: Router, 
     private documentService: DocumentService,
     ) { }

  ngOnInit(): void {

    this.ar.params.subscribe(pars => {
      let idAffaire = pars.pId;
      if (idAffaire != undefined) {
        this.affaireService.finAffaireById(idAffaire).subscribe(data => {
          this.affaire = data;

        })
      }
    }

    )

  }

  updateStatut(statutUpdate: number) {

    if (confirm("Êtes-vous sur de vouloir modifier le statut de cette affaire?")) {
      this.affaire.statut = statutUpdate;
      this.affaireService.updateAffaire(this.affaire).subscribe(data => {
        this.rt.navigate(['/accueil'])
      })
    }
  }



  // addDocument() {
  //   this.affaire.idAffaire
  //   // envoie vers le formulaire d'ajout de document )=> envoie vers un formulaire avec pour id l'utilisateur
  //   this.rt.navigate(['/document'], { queryParams: { id: this.affaire.idAffaire } })

  // }

  supprimer(idDocument: number) {

    if (confirm("Êtes-vous sur de vouloir supprimer ce document?")) {


      this.documentService.deleteDocument(idDocument).subscribe(data => {

        console.log("le document est bien suprimmer")
        location.reload();

      })
    }
  }

  supprimerAffaire() {

    if (confirm("Êtes-vous sûr de vouloir supprimer l'affaire?")) {
      this.affaireService.deleteAffaire(this.affaire.idAffaire).subscribe(data => {

        console.log("l'affaire a bien été supprimé");
        this.rt.navigate(["/accueil"])
      })
    }
  }

  

  addDocument() {
 
    this.documentService.addDocument(this.affaire.idAffaire, this.document).subscribe(data => {
      this.rt.navigate(["/accueil"]);
    })
  }


  displayForm() {
  
    this.modalBg.nativeElement.classList.add("bg-active");
  }

  hiddenForm() {
    this.modalBg.nativeElement.classList = "modal-bg";
  }

}
