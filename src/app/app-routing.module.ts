import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AffaireDetailsComponent } from './affaire/affaire-details/affaire-details.component';
import { AffaireComponent } from './affaire/affaire/affaire.component';
import { DocumentComponent } from './document/document.component';
import { TacheDetailsComponent } from './tache/tache-details/tache-details.component';
import { TacheComponent } from './tache/tache.component';
import { TribunalComponent } from './tribunal/tribunal.component';
import { UtilisateurComponent } from './utilisateur/utilisateur.component';

const routes: Routes = [
{path:'tâches', component:TacheComponent},
{path:'accueil', component:AffaireComponent},
{path:'affaireDetails', component:AffaireDetailsComponent},
{path:'tacheDetails', component:TacheDetailsComponent},
{path:'tribunals', component:TribunalComponent},
{path:'utilisateur', component:UtilisateurComponent},
{path:'document', component:DocumentComponent},
{path:'enregistrement', component:RegisterComponent},
{path:'connexion', component: LoginComponent},
{path:'home', component:HomeComponent},
{path:"profile", component:ProfileComponent},
{path: '', redirectTo: 'home', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
