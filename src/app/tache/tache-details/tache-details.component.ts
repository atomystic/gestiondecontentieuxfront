import { Utilisateur } from './../../_model/utilisateur';
import { AffaireService } from './../../_service/affaire.service';
import { Phase } from './../../_model/phase';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Tache } from 'src/app/_model/tache';
import { PhaseService } from 'src/app/_service/phase.service';
import { TacheService } from 'src/app/_service/tache.service';
import { UtilisateurService } from 'src/app/_service/utilisateur.service';

@Component({
  selector: 'app-tache-details',
  templateUrl: './tache-details.component.html',
  styleUrls: ['./tache-details.component.css']
})
export class TacheDetailsComponent implements OnInit {

  @ViewChild("modalBtn") modalBtn: ElementRef;
  @ViewChild("modalBg") modalBg: ElementRef;
  @ViewChild("modalBg2") modalBg2: ElementRef;
  @ViewChild("modalClose") modalClose: ElementRef;
  tache: Tache = new Tache();
  phase: Phase = new Phase();
  idTache: number;
  utilisateurs: Utilisateur[];
  utilisateur: Utilisateur;


  constructor(private ar: ActivatedRoute,
    private tacheService: TacheService,
    private rt: Router,
    private phaseService: PhaseService,
    private utilisateurService: UtilisateurService) { }

  ngOnInit(): void {
    this.ar.queryParamMap.subscribe(Param => {
      this.idTache = parseInt(Param.get("id"))
      if (this.idTache != undefined) {
        this.tacheService.findTacheById(this.idTache).subscribe(data => {
          this.tache = data;
        })

      }

    })

    this.utilisateurService.findAllUtilisateurs().subscribe(data => {

      this.utilisateurs = data
    })
    this.modalBg2.nativeElement.classList.add("bg-active");



  }// ngOnInit


  deleteTache() {

    console.log(this.tache)
    if (confirm("Etes-vous sûr de vouloir supprimer la tâche?")) {


      this.tacheService.deleteTache(this.tache.idTache).subscribe(data => {
        alert(`La tâche ${this.tache.titre} a bien été supprimée`)
        this.rt.navigate(['/tâches']);

      })
    }
  }

  addPhase() {
    this.phase.dateDebut = (new Date(this.phase.dateDebut))
    this.phase.dateFin = (new Date(this.phase.dateFin))
    this.phaseService.addPhaseWithTache(this.phase, this.idTache).subscribe(data => {
      location.reload();
    })

  }

  displayFormPhase() {
    this.modalBg.nativeElement.classList.add("bg-active");
  }
  hiddenFormPhase() {
    this.modalBg.nativeElement.classList = "modal-bg";
  }

  supprimerPhase(idPhase: number) {
    if (confirm("Etes-vous sûr de vouloir supprimer la phase?")) {
      this.phaseService.deletePhase(idPhase).subscribe(data => {

        location.reload();
      })

    }

  }

  displayFormUser() {

    this.modalBg2.nativeElement.classList.add("bg-active");
  }

  addUtilisateur() {
    this.tache.utilisateurs.push(this.utilisateur)
    console.log(this.tache.utilisateurs)
    this.tacheService.updateTache(this.tache).subscribe(data => {
      location.reload();

    })
  }


  hiddenFormUser() {
    this.modalBg2.nativeElement.classList = "modal-bg";
  }
  deleteUser(u: Utilisateur) {
    if(confirm(`Etes-vous sûr de vouloir supprimer la contribution de ${u.nomUtilisateur} ${u.prenomUtilisateur}?`)){

    }
    let i = 0;
    this.tache.utilisateurs.forEach(user => {

      if (user.idUtilisateur === u.idUtilisateur) {
        this.tache.utilisateurs.splice(i, 1);
        this.deleteReq();
      }
      i++

    })
  }

  deleteReq(){

this.tacheService.updateTache(this.tache).subscribe(data=>{

})
  }
}