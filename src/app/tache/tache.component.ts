import * as Chart from 'chart.js'
import { Tribunal } from './../_model/tribunal';
import { Tache } from './../_model/tache';
import { Component, ElementRef, OnInit, ViewChild, ɵConsole } from '@angular/core';
import { Router } from '@angular/router';
import { Calendar, CalendarOptions } from '@fullcalendar/core';
import { PhaseService } from '../_service/phase.service';
import { TacheService } from '../_service/tache.service';
import { AffaireService } from '../_service/affaire.service';
import { Affaire } from '../_model/affaire';
import { TribunalService } from '../_service/tribunal.service';
import { LocalizableString } from 'survey-angular';


@Component({
  selector: 'app-tache',
  templateUrl: './tache.component.html',
  styleUrls: ['./tache.component.css']
})
export class TacheComponent implements OnInit {
  tacheEtPhaseEvents: Object[] = [];
  colorPhase = ["#f64f5d", "#c38a74", "#24343c", "#9ea5a8", "#f5969b", "#bdc2c4", "#748c91", "#d2b5b2", "#eebec2"];
  calendarOptions: CalendarOptions;
  tache: Tache = new Tache();
  affaires: Affaire[];
  tribunals: Tribunal[];
  @ViewChild("modalBtn") modalBtn: ElementRef;
  @ViewChild("modalBg") modalBg: ElementRef;
  @ViewChild("modalClose") modalClose: ElementRef;
  canvas;
  public ctx: any;

  constructor(private tacheService: TacheService,
    private phaseService: PhaseService,
    private rt: Router,
    private affaireService: AffaireService,
    private tribunalService: TribunalService
  ) { }

  ngOnInit(): void {
    this.buildPhaseEvent();
  }

  // Etape 1: ngOnInit construction des events des phases
  buildPhaseEvent() {
    this.phaseService.findAllPhases().subscribe(data => {

      data.forEach(phase => {

        let nombre: number = Math.floor(Math.random() * 9);
        let phaseEvent = {
          backgroundColor: this.colorPhase[nombre],
          title: phase.tache.titre + " (Phase : " + phase.nom + ")",
          start: phase.dateDebut,
          end: phase.dateFin,
          idPhase: phase.idPhase,
          idTache: phase.tache.idTache,
        }

        this.tacheEtPhaseEvents.push(phaseEvent);
      })

      //Etape 2
      this.buildTacheEvent();
    })

  }


  // Etape 2: ngOnInit construction des events des taches
  buildTacheEvent() {
    this.tacheService.findAllTaches().subscribe(data1 => {
      let nombre: number = Math.floor(Math.random() * 9);
      data1.forEach(tache => {

        let tacheEvent: object = {
          backgroundColor: this.colorPhase[nombre],
          idTache: tache.idTache,
          date: tache.dateCreation,
          title: 'création de la  tâche' + ' "' + tache.titre + ' "',
          statutAudience: tache.statutAudience,
          description: tache.description
        }

        this.tacheEtPhaseEvents.push(tacheEvent);
      })

      // Etape 3
      this.buildCalendar();
  
    })

  }

  // Etape 3: ngOnInit construction des calendrier
  buildCalendar() {

    this.calendarOptions = {
      eventColor: "#24343c",
      events: this.tacheEtPhaseEvents,
      initialView: "timeGridWeek",
      locale: "fr",
      headerToolbar: {
        start: 'prev,next today',
        center: 'title',
        end: 'dayGridMonth,timeGridWeek,list'
      },
      buttonText: {
        today: "Aujourd'hui",
        month: "Mois",
        week: "Semaine",
        list: "Liste"
      },
      themeSystem: 'bootstrap',
      nowIndicator: true,
      editable: true,
      timeZone: "Europe/Paris",
      selectable: true, // on peut sdélectionner une plage horaire
      selectMirror: true, // ajoute un placeholder
      unselectAuto: true, // si nclique en dehors de la selection, prochaine selection est nettoyer
      select: (infos) => {
        console.log(infos)
      },
      eventClick: (infos) => {
        this.voirDetailsTache(infos);
      },
    }
    alert("Pour voir les détails d'une tâche, cliquez sur la tâche en question.")

  }

  displayForm() {
    this.affaireService.findAllAffaire().subscribe(data => {
      this.affaires = data;
    })
    this.tribunalService.findAllTribunal().subscribe(data => {
      this.tribunals = data
    })
    this.modalBg.nativeElement.classList.add("bg-active");
  }

  hiddenForm() {
    this.modalBg.nativeElement.classList = "modal-bg";
  }


  addTache() {
    this.tacheService.addTache(this.tache).subscribe(data => {
      alert(`La tâche ${this.tache.titre} a bien été ajouté`);
      location.reload();
    })
  }

  voirDetailsTache(infosEvents) {
    this.rt.navigate(['/tacheDetails'], { queryParams: { id: infosEvents.event.extendedProps.idTache } })
  }

}// fin de classe
