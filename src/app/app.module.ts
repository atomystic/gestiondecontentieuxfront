import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



// fullcalendar
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin, {Draggable} from '@fullcalendar/interaction';
import { formatDate } from '@fullcalendar/angular';
import timeGridPlugin from "@fullcalendar/timegrid";
import list from "@fullcalendar/list";

//, composant
import { AppComponent } from './app.component';
import { TacheDetailsComponent } from './tache/tache-details/tache-details.component';
import { TribunalComponent } from './tribunal/tribunal.component';
import { UtilisateurComponent } from './utilisateur/utilisateur.component';
import { DocumentComponent } from './document/document.component';
import { AffaireComponent } from './affaire/affaire/affaire.component';
import { AffaireDetailsComponent } from './affaire/affaire-details/affaire-details.component';
import { TacheComponent } from './tache/tache.component';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';


FullCalendarModule.registerPlugins([
  dayGridPlugin,
  interactionPlugin,
  timeGridPlugin,
  list,
]);


@NgModule({
  declarations: [
    AppComponent,
    TacheComponent,
    AffaireComponent,
    AffaireDetailsComponent,
    TacheDetailsComponent,
    TribunalComponent,
    UtilisateurComponent,
    DocumentComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    ProfileComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FullCalendarModule,
    

  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
