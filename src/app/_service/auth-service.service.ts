import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

const AUTH_API = "http://localhost:8080/rest/auth/";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class AuthServiceService {
  constructor(private http: HttpClient) { }


  login(username: String, password: String): Observable<any> {
    return this.http.post(AUTH_API + "signin", {
      username,
      password
    }, httpOptions)
  }

  register(username: String, email: String, password: String, name: String): Observable<any> {

    return this.http.post(AUTH_API + "signup", {
      username,
      password,
      email,
      name
    },httpOptions)
  }

}
