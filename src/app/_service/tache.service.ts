import { Tache } from './../_model/tache';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const URL = "http://localhost:8080/rest/tache/";
@Injectable({
  providedIn: 'root'
})
export class TacheService {

  constructor(private http: HttpClient) { }
  findAllTaches(): Observable<Tache[]> {
    return this.http.get<Tache[]>(URL + "findAllTaches");
  }

  findTacheById(idTache : number):Observable<Tache>{
    return  this.http.get<Tache>(URL+"findTacheById/"+idTache);
  }
  addTache(tache : Tache):Observable<Tache>{


    return this.http.post<Tache>(URL+"addTache",tache);
  }

  updateTache(tache : Tache):Observable<Tache>{


    return this.http.put<Tache>(URL+"updateTache",tache);
  }
  deleteTache(id :number){

    return this.http.delete(URL+"deleteTache/"+id,{observe :"response"})
  }

 

}

// findTacheById /updateTache