import { Phase } from './../_model/phase';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
const URL="http://localhost:8080/rest/phase/";
@Injectable({
  providedIn: 'root'
})
export class PhaseService {

  constructor(private http:HttpClient) { }


  findAllPhases():Observable<Phase[]>{

    return this.http.get<Phase[]>(URL+"findAllPhases");
  }
  addPhaseWithTache(phase :Phase, idTache : number):Observable<Phase>{

    return this.http.post<Phase>(URL+"addPhaseWithTache/"+idTache,phase);
  }
  deletePhase(idPhase : number):Observable<any>{

return this.http.delete<any>(URL+"deletePhase/"+idPhase,{observe :"response"})
  }
}




