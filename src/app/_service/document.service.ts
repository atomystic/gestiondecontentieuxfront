import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Document } from '../_model/document';


const URL = "http://localhost:8080/rest/document/";
@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(private http: HttpClient) { }

  addDocument(idAffaire:number , document: Document): Observable<Document> {
console.log(document)
    return this.http.post<Document>(URL + "addDocument/"+idAffaire, document)
  }

deleteDocument(id:number){

  return this.http.delete(URL+"deleteDocument/"+id,{observe:'response'});

}

  // /deleteDocument
}
