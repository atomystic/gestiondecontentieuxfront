import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AffaireComponent } from '../affaire/affaire/affaire.component';
import { Affaire } from '../_model/affaire';
const URL="http://localhost:8080/rest/affaire/";
@Injectable({
  providedIn: 'root'
})
export class AffaireService {

  constructor(private http:HttpClient) { }

  findAllAffaire():Observable<Affaire[]>{

    return this.http.get<Affaire[]>(URL+"findAllAffaire");
  }

  finAffaireById(idAffaire:number):Observable<Affaire>{

    return this.http.get<Affaire>(URL+"findAffaireById/"+idAffaire)
  }

  updateAffaire(affaire: Affaire):Observable<Affaire>{
   return this.http.put<Affaire>(URL+"updateAffaire",affaire);
  }

  deleteAffaire(id:number){

    return this.http.delete(URL+"deleteAffaire/"+id,{observe :'response'})
  }

  addAffaire(affaire:Affaire):Observable<Affaire>{

    return this.http.post<Affaire>(URL+"addAffaire",affaire)
  }
  // /addAffaire
}

