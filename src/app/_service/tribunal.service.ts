import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tribunal } from '../_model/tribunal';
const URL = "http://localhost:8080/rest/tribunal/";
@Injectable({
  providedIn: 'root'
})
export class TribunalService {

  constructor( private http:HttpClient) {}
  findAllTribunal():Observable<Tribunal[]>{

    return this.http.get<Tribunal[]>(URL+"findAllTribunal")
  }
}
