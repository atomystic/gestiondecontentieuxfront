import { Utilisateur } from './../_model/utilisateur';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


const URL = "http://localhost:8080/rest/utilisateur/"
@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  constructor(private http: HttpClient) { }

  findAllUtilisateurs(): Observable<Utilisateur[]> {

    return this.http.get<Utilisateur[]>(URL + "findAllUtilisateurs");
  }

  deleteUtilisateur(id: number) {

    return this.http.delete(URL + "deleteUtilisateur/" + id, { observe: "response" })
  }

  findUtilisateur(id: number): Observable<Utilisateur> {

    return this.http.get<Utilisateur>(URL + "findUtilisateur/" + id)
  }
  updateUtilisateur(utilisateur: Utilisateur): Observable<Utilisateur> {

    return this.http.put<Utilisateur>(URL + "updateUtilisateur", utilisateur)
  }

}
